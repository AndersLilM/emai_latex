\section{Topology optimization}
\label{sec:topopt}
 The general topology optimization problem used for the conducted examples is formulated on the form: 
 \begin{align}
 \left.\begin{array}{rlc}
  \underset{\bs{\gamma}\in \mathbb{R}^n}{\mathrm{min}}  :& \Psi(\bs{\gamma},\bs{S}_1(\bs{\gamma}))&  \\ 
  s.t. :& \mathbf{\bs{\mathrm{R}}_{0}({{\bs{\gamma},\bs{S}_0(\bs{\gamma})}})} = \bs{0} & \\ 
  : &\mathbf{\bs{\mathrm{R}}_{1}({{\bs{\gamma},\bs{S}_0(\bs{\gamma}),\bs{S}_1(\bs{\gamma}}})} )= \bs{0}   & \\
  :& f_j(\bs{\gamma}) \le0, & j = 1,...,m\\ 
  :& \gamma_{min} \le \gamma_e \le \gamma_{max},  & e = 1,...,n
 \end{array} \right\}
 \label{eq:topoptProb}
 \end{align}
 
 where $\Psi$ is the objective function. %, that one wish to minimize with respect to the design variables. 
 The design variables are subjected to a box constraint with lower bound $\gamma_{min}$ and upper bound $\gamma_{max}$, where $n$, in this work, is the same as the number of elements in the FE model. $f_j$ are the constraints with $m$ being the number of constraints.
 
 \subsection{Interpolation functions}
 
 The analysis is a two-phase, four physics case, entailing normal compressible elasticity, compressible inviscid hydrostatic fluid, and perfect conducting and dielectric materials. The material properties has to be continuously interpolated as a function of the design variable $\gamma \in \{0, 1\}$. It is chosen that $\gamma =1$ should represent the solid conducting material and $\gamma =0$ the acoustic insulating medium. The material property phases used for the optimization is presented in table \ref{tab:inttab}.
 A favored interpolations approach is the SIMP scheme, where intermediate densities are penalized by raising the design variable to a power larger than one. This approach shows however to be troublesome, considering vibrational problems, as the stiffness converges to zero, orders of magnitude faster than the mass. This results in artificial local modes due to high mass to stiffness ratios, discussed in \cite{Bendsoe2004,GIL2007}. This is partly avoided using the RAMP (Rational Approximation of Material Properties) interpolation scheme, proposed by \cite{Stolpe2001}, for the shear and bulk moduli, while the density is interpolated linearly. The SIMP scheme is used for the permittivities in order to effectively penalize intermediate design variables in relation to the potential field. The interpolation functions takes the form:
 

 

 \begin{align}
 \begin{split}
 &G(\gamma) = G_s \frac{\gamma}{1+(1-\gamma)n}+G_a \left(1-\frac{\gamma}{1+(1-\gamma)n}\right)\\
 &K(\gamma) = K_s \frac{\gamma}{1+(1-\gamma)n}+K_a \left(1-\frac{\gamma}{1+(1-\gamma)n}\right)\\
 &  {\rho}(\gamma) = ({\rho}_s-{\rho}_a) \gamma+{\rho}_a  \\[10pt]
& \tilde{\varepsilon}(\gamma) = (\tilde{\varepsilon}_s-\tilde{\varepsilon}_a) \gamma^q+\tilde{\varepsilon}_a \\
& {\varepsilon}(\gamma) = ({\varepsilon}_s-{\varepsilon}_a) \gamma^q+{\varepsilon}_a  
\end{split}
 \end{align}
 
A continuation approach is employed for the penalization parameters where the optimization problem is continuously solved for increasing $q$ and $n$. The idea is to start out with a smooth and well posed problem and then progressively increase the penalization pushing the design variables towards the extremities 0 and 1. % \cite{Alexandersen2013}.
 %\begin{small}
 %\begin{align}
 %\text{Structural/conductor }  (\gamma =1): \,&  K \equiv K_s, \quad G \equiv G_s, \quad %\rho \equiv \rho_s \quad \varepsilon \equiv \varepsilon_s \quad \zeta \equiv \zeta_s \\
  %\text{Acoustic/dielectric } (\gamma= 0):\, & K \equiv K_a, \quad G \equiv G_a, \quad  %\rho \equiv \rho_a  \quad \varepsilon \equiv \varepsilon_a \quad \zeta \equiv \zeta_a
 %\end{align}
 %\end{small}

 %Certain restrictions apply for choice of material properties in order to maintain a stable system during the optimization process. These values have been established trough trial and error, and are presented in table \ref{tab:inttab}. The shear modulus should be zero for an inviscid media. To avoid numerical instabilities a value $G_a\ll 1 $ is chosen. It was found that $G_a =  10^{-10} $ produces adequate results and avoids stability issues. Ideally the conductivity $\sigma_s$ should be an infinite number in order to satisfy the assumption of an ideal conductor. This will however cause the system to be ill-conditioned. It was found that $\sigma_s = {\varepsilon}_a \times 10^5$ is sufficient to fulfill the assumption. 
 
 
 %The permittivity $\varepsilon$ is a material related constant, and proportional to the electrostatic forces. In the dielectric media $\varepsilon_a = \varepsilon_0$ is used for the main part of the presented problems, where $\varepsilon_0 = 8.85 \times 10^{-12}$ F/m, is the permittivity of free space. For the special case of a perfect conductor $\varepsilon_s =\infty$, which is not a feasible choice. In references \cite{Yoon2008,Qian2013} the arbitrary value $\varepsilon_s=10$ F/m is used with little justification, other than being a material related constant. 
 
 %It is observed that the electric field converges rapidly towards zero when $\gamma>0$. Since the electrostatic forces are also proportional to the square of the electric field, $\varepsilon_s$ has very little influence on these. %This entails that: $\bs{\sigma}_E(\gamma=0) \gg \bs{\sigma}_E(\gamma>0) \approx 0$. This is depicted in a small study found appendix \ref{app:perm}.
 \begin{table*}[t]
 \centering
 \caption{Material properties used for solid/conductor and acoustic/dielectric phases during optimization.}
 \label{tab:inttab}
 \begin{small}
 \begin{tabular}{lllllll}
  \cline{2-7} \rowcolor{Cyan}
             \hline & \multicolumn{4}{l}{$\bs{u}/p$
               fomulation}                                                                                                                                                                                                                                            & & Electric equation                                                                             \\ \hline
          \rowcolor{Cyan}     & \begin{tabular}[c]{@{}l@{}} Bulk modulus \\ $K(\gamma)$\end{tabular} & \begin{tabular}[c]{@{}l@{}}Shear modulus\\  $G(\gamma)$\end{tabular} & \begin{tabular}[c]{@{}l@{}}Density \\ $\rho(\gamma)$\end{tabular} & \begin{tabular}[c]{@{}l@{}}Permittivity \\ $\varepsilon(\gamma)$\end{tabular} &  & \begin{tabular}[c]{@{}l@{}} General permittivity  \\ $\tilde{\varepsilon}(\gamma)$\end{tabular} \\ \rowcolor{Cyan} \hline
 $\gamma= 0 $ & $K_a$                                                               & $G_a \equiv 10^{-10} $                                                        & $\rho_a$                                                          & $\varepsilon_a  $                                                               &  & ${\varepsilon}_a $                                                                       \\ \rowcolor{Cyan} 
 $\gamma = 1$ & $K_s$                                                               & $G_s$                                                               & $\rho_s$                                                          & $\varepsilon_s$                                                               &  & $ {\varepsilon}_a \times 10^5$                                                              \\ \cline{1-7} 
 \end{tabular}
 \end{small}
 \end{table*}
 
 
  %\subsection{Filtering}
  
Sensitivity and density filtering schemes, see e.g. \cite{Sigmund2007a}, are used independently in this paper to regularize the problem and to avoid checker boarding and mesh dependent solutions. 

%\section{Measure of non-discreteness}

%Despite using the RAMP interpolation scheme, intermediate design values are still observed to produce artificial local modes. This problem could be resolved by using projection methods together with a density filter to obtain completely discrete designs. We choose a more untraditional method that has proven to work well for these problems with a sensitivity filter. The measure of non discreteness \cite{Sigmund2007a}  is used as a constraint:

%\begin{align}
%{M}_{nd} - {M}_{nd}^0 < 0, \qquad %
%{M}_{nd} = \frac{\sum\limits_{e=1}^{n}\gamma_e(1-\gamma_e) }{n} \times 100 \%
 %\end{align}

%After convergence of the initial problem, the constraint is made active and ${M}_{nd}^0 $ is gradually decreased by $10\%$ every $10$th iteration until ${M}_{nd}$ is below $0.1\%$. The discreteness does come with a small trade off in the objective function, which we accept in order to properly represent the pressure field.
 
 

 
 