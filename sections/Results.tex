\section{Numerical results}
\label{sec:results}
\subsection{Fixed electrode design for minimizing sound pressure}

The purpose of this example is to illustrate how the electromechanical tuning can be utilized to qualitatively alter the sound pressure field in an arbitrary domain at a fixed excitation frequency.  The system is driven by a vertical excitation of the beam supports, depicted in figure \ref{fig:GILstringAcouOpen}. The design domain (gray color) is in this example shielded from the acoustic domain by hard wall boundary conditions at the interface and the displacement d.o.f. are locked in the design domain. This approach can be considered similar to designing a non-deformable electrode inside a box. This means that the design domain is only indirectly coupled with the acoustic domain, trough the electrostatic interaction with the deformable electrode.
The objective is to minimize the pressure in the domain $\Omega_{\Psi}$ depicted in figure \ref{fig:GILstringAcouOpen}. The optimization is carried out at $7.9$ kHz with applied voltage $\phi^*= 0.046$ V. The optimization problem looks as follows:

            \begin{align}
            \begin{small}
             \left.\begin{array}{rlc}
              \underset{\bs{\gamma}\in \mathbb{R}^n}{\mathrm{min}}  :& \Psi(\bs{\gamma},\bs{S}_1(\bs{\gamma}))= \int_{\Omega_{\Psi}} |p|^2 \mathrm{d}\Omega &  \\ 
              s.t. :& \mathbf{\bs{\mathrm{R}}_{0}({{\bs{\gamma},\bs{S}_0(\bs{\gamma})}})} = \bs{0} & \\ 
                : &\mathbf{\bs{\mathrm{R}}_{1}({{\bs{\gamma},\bs{S}_0(\bs{\gamma}),\bs{S}_1(\bs{\gamma}}})} )= \bs{0}   & \\
              :&\frac{\sum_e \gamma_e v_e}{V}\le 0.2 &\\ 
              :& \gamma_{min} \le \gamma_e \le \gamma_{max},  & e = 1,...,n
             \end{array} \right\}
             \end{small}
             \label{eq:topoptDomainAcou_string}
             \end{align}
             
             Optimized topologies obtained with varying filter type and radii are found in figure \ref{fig:opt_slenderbeam}. The general trend for the presented topologies is that the gap distance between the two electrodes has been minimized and the surface area maximized to a certain degree. These designs maximizes the electrostatic forces working on the beam and hereby reduces the stiffness of the system. The reduced stiffness of the beam leads to a change in the characteristic of the acoustic field. The best performing topologies is obtained with a sensitivity filter with the smallest filter radius. The results also indicate that the problem is highly non-convex, i.e. the final design is highly dependent on filter type and optimization parameters.
            

\begin{figure}[h!]
%\hspace{0.22cm}
  \centering
  \def\svgwidth{280	pt} 
  \begin{footnotesize}
  \import{Pictures/Results/}{String_AcouOpendom.pdf_tex} 
    \end{footnotesize}
  \caption{A slender clamp-clamp beam inside an acoustic cavity. The clamped supports of the beam are harmonically excited with an amplitude $a$. A potential difference $\phi^*$ is applied between the design domain and the beam. Volume constraint is 20\%. Plane stress conditions, with $E = 10^6$, $K_a = 0.6 $, $\nu = 0.17$, $\rho_s = 1$, $\rho_a = 1$, $\varepsilon_a = \varepsilon_0$, $\varepsilon_s = \varepsilon_0 \times 10$} 	
  \label{fig:GILstringAcouOpen}
  \end{figure}
  
  
  \begin{figure*}[h!]
  \begin{subfigure}[b]{1\textwidth}
    \begin{subfigure}[b]{0.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{Pictures/Results/initial}
  %\caption{$\psi = 8.9370 \,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
  \label{fig:initial}
  \end{subfigure}
  \begin{subfigure}[b]{0.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{Pictures/Results/Pot_initial}
  %\caption{$\psi = 11.8767\,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
  \label{fig:pot_initial}
  \end{subfigure}
  \caption{Initial uniform design guess with 20\% volume fraction. Objective: $\psi = 26.8210 \,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
  \end{subfigure}
  % % % % % %
  \begin{subfigure}[b]{1\textwidth}
  \begin{subfigure}[b]{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{Pictures/Results/R1sens_2}
%\caption{$\psi = 8.9370 \,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
\label{fig:R1sens}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{Pictures/Results/Pot_R1}
%\caption{$\psi = 11.8767\,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
\label{fig:pot_R1}
\end{subfigure}
\caption{Filter: sensitivity with $R = 0.75 \mathrm{\mu m }$. Objective: $\psi = 8.9370 \,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
\end{subfigure}
% % % % %
\begin{subfigure}[b]{1\textwidth}
\begin{subfigure}[b]{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{Pictures/Results/R2sens}
%\caption{$\psi = 9.9409 \,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
\label{fig:R2sens}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{Pictures/Results/Pot_R2}
%\caption{$\psi = 11.8767\,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
\label{fig:pot_R2}
\end{subfigure}
\caption{Filter: sensitivity with $R = 1.5 \mathrm{\mu m }$. Objective: $\psi = 9.9409  \,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
\end{subfigure}
% % % % % %
\begin{subfigure}[b]{1\textwidth}
\begin{subfigure}[b]{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{Pictures/Results/R1dens}
%\caption{$\psi = 11.3524\,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
\label{fig:R1dens}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{Pictures/Results/Pot_R1dens}
%\caption{$\psi = 11.8767\,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
\label{fig:pot_R1dens}
\end{subfigure}
\caption{Filter: density with $R = 0.75 \mathrm{\mu m }$. Objective: $\psi = 11.3524 \,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
\end{subfigure}
% % % % % % %
\begin{subfigure}[b]{1\textwidth}
\begin{subfigure}[b]{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{Pictures/Results/R2dens}
%\caption{$\psi = 11.8767\,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
\label{fig:R2dens}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{Pictures/Results/Pot_R2dens}
%\caption{$\psi = 11.8767\,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
\label{fig:pot_R2dens}
\end{subfigure}
\caption{Filter: density with $R = 1.5 \mathrm{\mu m }$. Objective: $\psi = 11.8767 \,  \mathrm{ \mu Pa} ^2 \mu \mathrm{m} ^2$}
\end{subfigure}
\caption{Optimized toplogies obtained with vayring filter type and radius. Left column: Sound pressure level (displacements amplitudes scaled 1000:1). Right column: Electric potential  (displacements scaled 10:1)}
\label{fig:opt_slenderbeam}
\end{figure*}
\FloatBarrier


\subsection{Transducer design}
In the preceding example the design domain was separated from the acoustic domain, thus the acoustic-structure interaction was not reflected in the design domain and the use of the $\bs{u}/p$ formulation can not fully be justified. 

In this example a fully coupled problem is examined. We consider the submerged cantilever beam shown in figure \ref{fig:GILbeam}. A potential is applied to the beam supports and the lower boundary is grounded. An input pressure is applied at the upper boundary. The remaining outer boundaries are constrained, depicting hard wall conditions. The solid and acoustic material properties are chosen to depict steel and air respectively. For the analysis the material properties $(G,\,K,\,\rho)$ are scaled down by a factor of $10^5$ in order to maintain a properly scaled system matrix.
\begin{figure}[h!]
%\hspace{0.22cm}
  %\centering
  \def\svgwidth{240	pt} 
  \begin{footnotesize}
  \import{Pictures/Results/}{Problem3.pdf_tex} 
    \end{footnotesize}
  \caption{Cantilever beam inside an acoustic cavity with a prescribed input pressure. A potential difference $\phi^*$ is applied between the lower boundary and the beam. Volume constraint is 30\%. Plane stress conditions, with $E = 200\times 10^9$, $K_a = 1.42 \times 10^5 $, $\nu = 0.33$, $\rho_s = 7000$, $\rho_a = 1.293$, $\varepsilon_a = \varepsilon_0$, $\varepsilon_s = \varepsilon_0 \times 10$. Volume constraint is 30\%} 	
  \label{fig:GILbeam}
  \end{figure}

%\subsubsection{Example 1}
%For the first example the objective is to minimize the sound pressure in the sub domain $\Omega_\Psi$. The optimization is conducted for three different levels of voltage yielding tree different designs. The optimization problem is:


            
             
   
THIRD PROBLEM
            \begin{align}
            \begin{small}
             \left.\begin{array}{rlc}
              \underset{\bs{\gamma}\in \mathbb{R}^n}{\mathrm{min}}  :& \Psi(\bs{\gamma},\bs{S}_1(\bs{\gamma}))= - \int_{\Omega_{\Psi}} |p_0 - p_V|^2 \mathrm{d}\Omega &  \\ 
              s.t. :& \mathbf{\bs{\mathrm{R}}_{0}({{\bs{\gamma},\bs{S}_0(\bs{\gamma})}})} = \bs{0} & \\ 
                : &\mathbf{\bs{\mathrm{R}}_{1}({{\bs{\gamma},\bs{S}_0(\bs{\gamma}),\bs{S}_1(\bs{\gamma}}})} )= \bs{0}   & \\
              :& \gamma_{min} \le \gamma_e \le \gamma_{max},  & e = 1,...,n
             \end{array} \right\}
             \end{small}
             \label{eq:topoptDomainAcou_string}
             \end{align}   


